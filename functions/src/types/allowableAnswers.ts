import { listTypeGuard } from "./generic";

export type QuestionAnswer = {
  [answer: string]: string[];
};

export const isQuestionAnswers = (value: unknown): value is QuestionAnswer => {
  if (!value || typeof value !== "object") return false;
  const record = value as Record<string, unknown>;
  return listTypeGuard(
    Object.values(record)[0],
    (val: unknown): val is string => {
      return typeof val === "string";
    }
  );
};

//insipired by https://github.com/sindresorhus/leven

export const answerSimilarity = (
  correctAnswer: string,
  submittedAnswer: string
): number | undefined => {
  const array = [];
  const correctAnswerCharacters = [];

  const longest =
    correctAnswer.length >= submittedAnswer.length
      ? correctAnswer
      : submittedAnswer;
  const shortest =
    correctAnswer.length >= submittedAnswer.length
      ? submittedAnswer
      : correctAnswer;

  // Suffix trimming where the strings are the same.
  let longestLength = longest.length;
  let shortestLength = shortest.length;

  while (
    longestLength > 0 &&
    longest.charCodeAt(~-longestLength) ===
      shortest.charCodeAt(~-shortestLength)
  ) {
    longestLength--;
    shortestLength--;
  }

  // Performing prefix trimming
  let start = 0;
  while (
    start < longestLength &&
    longest.charCodeAt(start) === shortest.charCodeAt(start)
  ) {
    start++;
  }

  longestLength -= start;
  shortestLength -= start;

  if (longestLength === 0) {
    return shortestLength;
  }

  let submittedCharacter;
  let result;
  let temporary;
  let temporary2;
  let correctAnswerIndex = 0;
  let submittedAnswerIndex = 0;

  while (correctAnswerIndex < longestLength) {
    correctAnswerCharacters[correctAnswerIndex] = longest.charCodeAt(
      start + correctAnswerIndex
    );
    array[correctAnswerIndex] = ++correctAnswerIndex;
  }

  while (submittedAnswerIndex < shortestLength) {
    submittedCharacter = shortest.charCodeAt(start + submittedAnswerIndex);
    temporary = submittedAnswerIndex++;
    result = submittedAnswerIndex;

    for (
      correctAnswerIndex = 0;
      correctAnswerIndex < longestLength;
      correctAnswerIndex++
    ) {
      temporary2 =
        submittedCharacter === correctAnswerCharacters[correctAnswerIndex]
          ? temporary
          : temporary + 1;
      temporary = array[correctAnswerIndex];
      // eslint-disable-next-line no-multi-assign
      result = array[correctAnswerIndex] =
        temporary > result
          ? temporary2 > result
            ? result + 1
            : temporary2
          : temporary2 > temporary
          ? temporary + 1
          : temporary2;
    }
  }

  return result;
};

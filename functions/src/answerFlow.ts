import {
  broadcastMessage,
  getAllBots,
  getBotForTeam,
  getPlayersTeam,
  getValFromDb,
  moveToNextRound,
  pushValToDb,
  setValInDb,
} from "./db";
import { answerSimilarity } from "./similarity";
import { Answers, isAnswers } from "./types/answer";

export const incrementTeamsScore = async (
  meetingId: string,
  teamId: string,
  scoreIncrement: number,
  timestamp: number
): Promise<void> => {
  await pushValToDb(
    `data/plugins/trivialeaderboardoverlay/${meetingId}/scoreEvents`,
    {
      amount: scoreIncrement,
      teamId,
      timestamp,
    }
  );
};

export const updateTeamScore = async (
  meetingId: string,
  timestamp: any,
  teamId: string
): Promise<number> => {
  const questionWeight = await getValFromDb(
    `/config/${meetingId}/current/currentState/plugins/trivialightninground/questionWeight`
  );
  await incrementTeamsScore(
    meetingId,
    teamId,
    typeof questionWeight === "number" ? questionWeight : 1,
    timestamp
  );
  return typeof questionWeight === "number" ? questionWeight : 1;
};

// checks if a answer has already been submitted
export const answerAlreadySubmitted = (
  previousAnswers: Answers,
  submitedAnswer: string
): boolean => {
  return (
    // check if each of the previous answer matches submitted
    Object.entries(previousAnswers)
      .map(([, answer]) => {
        if (
          answer.answer.toLowerCase().trim() ===
          submitedAnswer.toLocaleLowerCase().trim()
        ) {
          return true;
        }
        return false;
      })
      // if no answer matches answer has never been submitted
      .find((answered) => answered) !== undefined
  );
};

/**
 * Saves the answer in the db
 * Increments the team's score
 * Send the team a message
 */
export const saveAnswerAndIncrementScore = async (
  meetingId: string,
  teamId: string,
  teamBotId: string,
  roundName: unknown,
  messageContent: string,
  timestamp: any,
  msgSender: string,
  msgSenderName: string,
  teamMessage: string,
  decrementQuestionWeight: boolean
) => {
  const answerValue = await updateTeamScore(meetingId, timestamp, teamId);

  // store answer in db
  await pushValToDb(
    `data/plugins/lightning-round/${meetingId}/${roundName}/answers/${teamId}`,
    {
      senderId: msgSender,
      senderName: msgSenderName,
      answer: messageContent,
      timestamp,
      teamId,
      answerValue,
    }
  );

  if (decrementQuestionWeight) {
    await handleDecrementQuestionWeight(meetingId);
  }

  await broadcastMessage(meetingId, teamBotId, teamMessage);
};

export const handleDecrementQuestionWeight = async (meetingId: string) => {
  // get the question weight value
  const questionWeight = await getValFromDb(
    `/config/${meetingId}/current/currentState/plugins/trivialightninground/questionWeight`
  );

  // decrement the weight if exists and greater than 0
  if (
    questionWeight &&
    typeof questionWeight === "number" &&
    questionWeight > 0
  ) {
    await setValInDb(
      `/config/${meetingId}/current/currentState/plugins/trivialightninground/questionWeight`,
      questionWeight - 1
    );
  }
};

/**
 * Handles a correct answer submission from a team
 * @param meetingId the zoomsense meeting id
 * @param messageContent the submitted answer
 * @param timestamp the current timestamp
 * @param msgSender the id of the sender of the message
 * @param msgSenderName the name of the sender of the message
 */
export const handleCorrectAnswer = async (
  meetingId: string,
  messageContent: string,
  timestamp: any,
  msgSender: string,
  msgSenderName: string
) => {
  // get team info
  const teamId = await getPlayersTeam(msgSenderName.toString(), meetingId);
  if (!teamId) return;
  const teamBotId = await getBotForTeam(meetingId, teamId);
  if (!teamBotId) return;

  // get round info
  const roundName = await getValFromDb(
    `/config/${meetingId}/current/currentState/plugins/trivialightninground/roundName`
  );
  const roundType = await getValFromDb(
    `/config/${meetingId}/current/currentState/plugins/trivialightninground/roundType`
  );
  if (typeof roundType !== "string") {
    return;
  }

  // handle answer based on round type
  switch (roundType) {
    case "single_answer": {
      await correctAnswerSingle(
        meetingId,
        teamId,
        teamBotId,
        roundName,
        messageContent,
        timestamp,
        msgSender,
        msgSenderName
      );
      break;
    }
    case "race_round": {
      await correctAnswerRaceRound(
        meetingId,
        teamId,
        teamBotId,
        roundName,
        messageContent,
        timestamp,
        msgSender,
        msgSenderName
      );
      break;
    }
    case "trivia_question": {
      await correctAnswerMultiple(
        meetingId,
        teamId,
        roundName,
        messageContent,
        timestamp,
        msgSender,
        msgSenderName,
        teamBotId
      );
      break;
    }
    default: {
      console.error("Unsupported round type:", roundType);
    }
  }
};

/**
 * Handles a correct answer submission for the multiple answer type
 * @param meetingId the zoomsense meeting id
 * @param teamId the id of the team which submitted the answer
 * @param teamBotId the id of the bot representing this team
 * @param roundName the name of the current question
 * @param messageContent the submitted answer
 * @param timestamp the current timestamp
 * @param msgSender the id of the sender of the message
 * @param msgSenderName the name of the sender of the message
 */
export const correctAnswerMultiple = async (
  meetingId: string,
  teamId: string,
  roundName: unknown,
  messageContent: string,
  timestamp: any,
  msgSender: string,
  msgSenderName: string,
  teamBotId: string
) => {
  var previousAnswers = await getValFromDb(
    `data/plugins/lightning-round/${meetingId}/${roundName}/answers/${teamId}`
  );

  // if team has previously submitted this answer -> let team know
  if (
    isAnswers(previousAnswers) &&
    answerAlreadySubmitted(previousAnswers, messageContent)
  ) {
    await broadcastMessage(
      meetingId,
      teamBotId,
      `${messageContent} has already been submitted, please try a different answer`
    );
    return;
  }

  // team hasnt submitted this answer yet -> increment score
  await saveAnswerAndIncrementScore(
    meetingId,
    teamId,
    teamBotId,
    roundName,
    messageContent,
    timestamp,
    msgSender,
    msgSenderName,
    `Well done! ${messageContent} was a correct answer`,
    false
  );
};

/**
 * Handles a correct answer submission for the race round answer type
 * @param meetingId the zoomsense meeting id
 * @param teamId the id of the team which submitted the answer
 * @param teamBotId the id of the bot representing this team
 * @param roundName the name of the current question
 * @param messageContent the submitted answer
 * @param timestamp the current timestamp
 * @param msgSender the id of the sender of the message
 * @param msgSenderName the name of the sender of the message
 */
export const correctAnswerRaceRound = async (
  meetingId: string,
  teamId: string,
  teamBotId: string,
  roundName: unknown,
  messageContent: string,
  timestamp: any,
  msgSender: string,
  msgSenderName: string
) => {
  var previousAnswers = await getValFromDb(
    `data/plugins/lightning-round/${meetingId}/${roundName}/answers/${teamId}`
  );

  // if team has previously submitted this answer -> let team know
  if (
    isAnswers(previousAnswers) &&
    answerAlreadySubmitted(previousAnswers, messageContent)
  ) {
    await broadcastMessage(
      meetingId,
      teamBotId,
      "You've already gotten points for this question, sit back and relax while the other teams are figuring it out"
    );
    return;
  }

  // save answer, increment score and let team know
  await saveAnswerAndIncrementScore(
    meetingId,
    teamId,
    teamBotId,
    roundName,
    messageContent,
    timestamp,
    msgSender,
    msgSenderName,
    `Well done! ${messageContent} was the correct answer, you can relax while other teams are still figuring it out`,
    true
  );
};

/**
 * Handles a correct answer submission for the single answer type
 * @param meetingId the zoomsense meeting id
 * @param teamId the id of the team which submitted the answer
 * @param teamBotId the id of the bot representing this team
 * @param roundName the name of the current question
 * @param messageContent the submitted answer
 * @param timestamp the current timestamp
 * @param msgSender the id of the sender of the message
 * @param msgSenderName the name of the sender of the message
 */
export const correctAnswerSingle = async (
  meetingId: string,
  teamId: string,
  teamBotId: string,
  roundName: unknown,
  messageContent: string,
  timestamp: any,
  msgSender: string,
  msgSenderName: string
) => {
  // save answer, increment score and let team know
  await saveAnswerAndIncrementScore(
    meetingId,
    teamId,
    teamBotId,
    roundName,
    messageContent,
    timestamp,
    msgSender,
    msgSenderName,
    `Well done! ${messageContent} was the correct answer, we are moving on to the next round`,
    false
  );

  // message all other teams  the correct answer
  const botIds = await getAllBots(meetingId);
  botIds.forEach(async (id) => {
    // if is winning team's bot ignore
    if (id === teamBotId) {
      return;
    }
    await broadcastMessage(
      meetingId,
      id,
      `Unfortunately, Team ${teamId} guessed the correct answer (${messageContent}), better luck next time!`
    );
  });

  await moveToNextRound(meetingId, timestamp);
};

export const checkAnswerSimilarity = async (
  answer: string,
  submittedAnswer: string,
  meetingId: string,
  teamBotId: string
) => {
  const similarity = answerSimilarity(answer, submittedAnswer);
  if (similarity && similarity < 3) {
    await broadcastMessage(meetingId, teamBotId, "This answer was very close!");
    return;
  }
};

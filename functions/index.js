const admin = require("firebase-admin");
admin.initializeApp();

const { lightningAnswers } = require("./lib/lightningAnswers");

exports.lightningAnswers = lightningAnswers;

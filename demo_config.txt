name: _default
tags:
  - Default
schedule:
  script:
    - name: Single_Answer_Example
      advance:
        advance:
        type: manual        
      plugins:
        - plugin: lightning-round
          settings:
            roundName: "question_1"
            roundType: "single_answer"
            solutions: 
              "correct answer" : ["correct_answer_q1", "correct", "q1","correct answer"]
            questionWeight: 1
    - name: Trivia_Answer_Example
      advance:
        type: manual
      plugins:
        - plugin: lightning-round
          settings:
            roundName: "question_2"
            roundType: "trivia_question"
            solutions: 
              "correct answer 2" : ["correct_answer_q2", "correct2", "q2", "correct answer 2"]
            questionWeight: 1
    - name: Race_Answer_Example
      advance:
        type: manual
      plugins:
        - plugin: lightning-round
          settings:
            roundName: "question_3"
            roundType: "race_round"
            solutions: 
              "correct answer 3" : ["correct_answer_q3", "correct3", "q3", "correct answer 3"]
            questionWeight: 10
 

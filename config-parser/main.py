import argparse

import pyperclip

from tools import get_data, parse_file, get_round_yaml, save_rounds_as_config


def parse_args():
    parser = argparse.ArgumentParser(description='Build a config file from csv template')
    parser.add_argument('data_path', metavar='D', type=str,
                        help='the path to load the data from, should either be a google sheets url or a path to a csv file')
    parser.add_argument('--save_file_name', type=str, default=None, required=False,
                        help='the yaml filename to save the config to')
    parser.add_argument('--extra_plugins', type=str, default=None, nargs="+",
                        help='list of plugin names to be included for each round')
    parser.add_argument('--num_teams', type=int, default=10,
                        help='the number of teams in the trivia, this is required for race round type of questions, where the question weight will be set to the number of team.')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    round_data = get_data(args.data_path)

    rounds, errors = parse_file(round_data, **vars(args))
    print("------------------------")
    print(f"Loaded csv and successfully parsed {len(rounds)} Trivia rounds!")
    if len(errors) > 0:
        print(f"Errors occurred ({len(errors)}):")
        [print(error)
         for error in errors]
    print("------------------------")

    yml = get_round_yaml(rounds)
    pyperclip.copy(yml)
    print("Rounds yml copied to clipboard!")

    if args.save_file_name is not None:
        save_rounds_as_config(rounds, args.save_file_name)

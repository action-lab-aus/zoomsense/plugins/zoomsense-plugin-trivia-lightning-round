# Python Parser for Trivia lightning round config file

This is a Python tool that will allow you to build a trivia lightning round config file from a csv file.  
This will make your life a lot easier
It can either read data from a publicly available google sheet or a local csv file
It will copy the resulting yml to the clipboard and can save it as a yml file

### Environment setup
`conda env create -f environment.yml`

### Usage

#### Arguments
- first positional argument: Data path : either a Google sheets url or a path to csv file
- num_teams: [Optional] the number of teams (for race_round) (default = 10) 
- save_file_name: [Optional] a filename to save a .yml file to


**From Google Sheets url:**  
`python main.py https://docs.google.com/spreadsheets/d/1Tm_bne9mUF4ESkm61aLwxZnHJXHpXQPqVPsgYiydVFA/edit?usp=sharing --num_teams 10`

**From Local csv File:**
`python main.py demo_config.csv --num_teams 10`

The demo_config.csv file contains an example of how to use this function  
The csv file must contain the following columns:
1) Question Number (eg 1)
2) Question Name (eg 'Question 1')
3) Question Type (eg 'race_round')
4) The Question Weight (eg 5)
5) Each Proceeding row will be treated as a Question Answer
   1) This will be in the form of a comma separated list
   2) The first value of this list will be treated as the answer's name


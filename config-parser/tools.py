import os
import re
from os.path import exists

import pandas as pd
import requests

from TriviaRound import TriviaRound


def parse_file(data, **kwargs):
    errors = []
    questions = []
    for i, round in enumerate(data.iterrows()):
        try:
            questions.append(TriviaRound(round[1], **kwargs))
        except Exception as e:
            errors.append(f"{i} - {e}")
    return questions, errors


def load_file(filename):
    try:
        data = pd.read_csv(filename)
        print("Successfully loaded data from csv file")
        return data
    except:
        raise Exception(f"Error loading csv file, are you sure it exists? {filename}")


def get_data_from_google_sheet(url):
    res = requests.get(url=url)
    google_sheet_filename = "google_sheet_data.csv"
    with open(google_sheet_filename, 'wb') as f:
        f.write(res.content)
    try:
        data = load_file(google_sheet_filename)
        print("Successfully loaded data from google sheet")
        os.remove(google_sheet_filename)
        return data
    except Exception as e:
        raise Exception(f"Error parsing google sheets ({e})")


def get_data(data_path):
    def path_is_url(path):
        return "http" in path

    def url_to_sheet_download_url(path):
        id_regex = re.match(r"https://docs.google.com/spreadsheets/d/(?P<id>.+)/edit.+", path)
        if id_regex is None:
            return None
        return f"https://docs.google.com/spreadsheets/d/{id_regex.groupdict()['id']}/export?format=csv"

    def path_is_file(path):
        try:
            return exists(path)
        except:
            return False

    if path_is_url(data_path):
        sheet_url = url_to_sheet_download_url(data_path)
        if sheet_url is None:
            raise Exception(
                "Url Data path needs to be a google sheets share link (available to public) in the form 'https://docs.google.com/spreadsheets/d/{sheet_id}/edit?usp=sharing'")
        return get_data_from_google_sheet(sheet_url)

    if not path_is_file(data_path):
        raise Exception("Data path must either be a url or a file path")

    return load_file(data_path)


def save_rounds_as_config(yml, filename):
    try:
        with open(filename, "w") as f:
            f.write(yml)
        print(f"Successfully saved config yaml to {filename}")
    except Exception as e:
        print(f"Error saving config file: {e}")


def get_round_yaml(rounds):
    yml = ""
    for round in rounds:
        yml += str(round)
    return yml

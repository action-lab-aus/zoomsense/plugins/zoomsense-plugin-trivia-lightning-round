from settings import ROUND_TYPES


class TriviaRound:
    def __init__(self, data, extra_plugins=None, num_teams=10, **kwargs):
        self.index = None
        self.name = None
        self.round_type = None
        self.solutions = None
        self.weight = None
        self.time = None
        self.extra_plugins = extra_plugins
        self._parse_round(data, num_teams)

    @staticmethod
    def _handle_index(index):
        try:
            return int(index)
        except:
            raise Exception(f"Error parsing Question index, this value needs to be an integer but was '{index}'")

    @staticmethod
    def _handle_name(name):
        if name is None:
            raise Exception(f"Error parsing Question Name, this value needs to be a string but was '{name}'")
        return name

    @staticmethod
    def _handle_type(type):
        if type not in ROUND_TYPES:
            raise Exception(f"Error parsing Round type, needs to be one of {ROUND_TYPES}, but was {type}")
        return type

    def _handle_weight(self, weight, num_teams):
        if self.round_type == "race_round":
            return num_teams
        try:
            return float(weight)
        except:
            raise Exception(f"Error parsing Question Weight, needs to be a float but was {weight}")

    def _handle_solutions(self, solutions_data):
        solutions = []
        for solution in solutions_data:
            if solution is not None and str(solution) != "" and str(solution) != 'nan':
                allowed_answers = solution.split(", ")
                solutions.append({"name": allowed_answers[0], "allowed_answers": allowed_answers})

        if len(solutions) == 0:
            raise Exception(f"Question must have at least one answer, got: {solutions_data}")
        if self.round_type == "single_answer" and len(solutions) > 1:
            raise Exception(f"Question is of type single_answer but has multiple solutions")

        return solutions

    def _handle_time(self, data):
        try:
            return int(data)
        except:
            raise Exception(f"Error parsing Question Time, needs to be an int but was {data}")

    def _parse_round(self, data, num_teams=10):
        self.index = self._handle_index(data["Question Id"])
        self.name = self._handle_name(data["Question Name"])
        self.round_type = self._handle_type(data["Question Type"])
        self.weight = self._handle_weight(data["Question Weight"], num_teams)
        self.time = self._handle_time(data["Question Time"])
        self.solutions = self._handle_solutions(data.iloc[5:])

    @staticmethod
    def _solution_to_str(solution):
        allowed_answers_string = ", ".join([f'"{answer}"' for answer in solution["allowed_answers"]])
        return f'"{solution["name"]}" : [ {allowed_answers_string} ]'

    def _solutions_to_str(self):
        solutions_str = [f"{self._solution_to_str(solution)}" for solution in self.solutions]
        return "\n          ".join(solutions_str)

    def _render_extra_plugins(self):
        if self.extra_plugins is None: return ""
        extra_plugins = [f"- plugin: {extra_plugin}" for extra_plugin in self.extra_plugins]
        return "\n    ".join(extra_plugins)

    def __str__(self):
        return f"""- name: {self.name}
  advance:
    type: manual
  plugins:
    - plugin: trivialightninground
      settings:
        roundName: "question_{self.index}"
        roundType: "{self.round_type}"
        questionWeight: {self.weight}
        questionTime: {self.time}
        solutions: 
          {self._solutions_to_str()}
    {self._render_extra_plugins()}
"""


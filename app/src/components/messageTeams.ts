import { ZoomSenseClientFirebaseDB } from "@zoomsense/zoomsense-firebase";
import { SubmittedAnswer, TeamAnswers } from "./types";

export const getCurrentQuestionTime = async (
  meetingId: string,
  db: ZoomSenseClientFirebaseDB
) => {
  return (
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/trivialightninground/questionTime`
      )
      .once("value")
  ).val();
};
export const getCurrentQuestionId = async (
  meetingId: string,
  db: ZoomSenseClientFirebaseDB
): Promise<string | null> => {
  return (
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/trivialightninground/roundName`
      )
      .once("value")
  ).val();
};
export const getTeamSubmittedAnswers = (
  answers: SubmittedAnswer
): TeamAnswers[] => {
  return Object.entries(answers).map(([teamId, answers]) => {
    return {
      teamId,
      answers: Object.entries(answers).map(([, answer]) => answer.answer),
      score: Object.entries(answers)
        .map(([, answer]) => answer.answerValue)
        .reduce((prevSum, nexVal) => prevSum + nexVal, 0),
    };
  });
};

export const messageAllTeams = async (
  meetingId: string,
  db: ZoomSenseClientFirebaseDB,
  teamAnswers: TeamAnswers[]
) => {
  const teamsWithAnswers: string[] = [];

  // message teams who answered with their answers and score
  await teamAnswers.forEach(async ({ teamId, answers, score }) => {
    const botId = await getBotForTeam(meetingId, teamId, db);
    if (!botId) {
      return;
    }
    teamsWithAnswers.push(botId);
    const content = `The round is over! \nYou got the following answers for this round: \n  - ${answers.join(
      "\n  - "
    )} \n Your total score for this round was ${score}`;
    await broadcastMessage(meetingId, botId, content, db);
  });

  // message teams who didnt answer anything
  const teamsWithNoAnswer = (await getAllBots(meetingId, db)).filter(
    (botId) => !teamsWithAnswers.includes(botId)
  );
  await teamsWithNoAnswer.forEach(async (botId) => {
    await broadcastMessage(
      meetingId,
      botId,
      "The round is over! \nYou got no points this round, better luck next time",
      db
    );
  });
};

/**
 * Returns a zoom bot id associated with a particular team
 * @param meetingId The meeting id
 * @param teamId the team id
 * @returns the bot id
 */
export const getBotForTeam = async (
  meetingId: string,
  teamId: string,
  db: ZoomSenseClientFirebaseDB
): Promise<string | null> => {
  const team = (
    await db.ref(`data/plugins/teamPlugin/${meetingId}/${teamId}`).once("value")
  ).val();
  if (!team) {
    return null;
  }
  return team.sensorId;
};

export const getAllBots = async (
  meetingId: string,
  db: ZoomSenseClientFirebaseDB
): Promise<string[]> => {
  const activeSpeakers = (
    await db.ref(`/data/chats/${meetingId}`).once("value")
  ).val();

  if (!activeSpeakers || typeof activeSpeakers !== "object") return [];

  return Object.entries(
    (activeSpeakers as Record<string, { isInBO: boolean }>) ?? {}
  )
    .filter(([, { isInBO }]) => isInBO)
    .map(([zoomSensor]) => zoomSensor);
};

// Request that sensor with ID `sensorId` broadcasts the given message
// (i.e. sends it to everyone in the room)
export const broadcastMessage = async (
  meetingId: string,
  sensorId: string,
  content: string,
  db: ZoomSenseClientFirebaseDB
): Promise<void> => {
  await db.ref(`data/chats/${meetingId}/${sensorId}/message`).push({
    msg: content,
    receiver: 0,
  });
};

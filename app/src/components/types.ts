export const listTypeGuard = <T>(
  l: unknown,
  typeGuard: (val: unknown) => val is T
): l is T[] => {
  if (!l || typeof l !== "object" || !Array.isArray(l)) return false;
  return l.every(typeGuard);
};

export type Answer = {
  answer: string;
  senderId: string;
  senderName: string;
  teamId: string;
  timestamp: number;
  answerValue: number;
};

export type SubmittedAnswer = {
  [teamId: string]: {
    [answerId: string]: Answer;
  };
};

export type TeamAnswers = {
  teamId: string;
  score: number;
  answers: string[];
};

export type User = {
  username: string;
  userRole: string;
  userId: string;
};
export type Team = {
  isInBO: boolean;
  sensorId: string;
  teamName: string;
  members: User[];
};

export type Teams = {
  [teamId: string]: Team;
};

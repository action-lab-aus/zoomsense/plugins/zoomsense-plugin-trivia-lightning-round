import { SubmittedAnswer, Teams } from "./types";
import { ZoomSensePlugin } from "@zoomsense/zoomsense-firebase";

declare module "@zoomsense/zoomsense-firebase" {
  interface ZoomSenseConfigCurrentStatePlugins {
    trivialightninground: {
      roundName: string;
      questionTime: number;
    } & ZoomSensePlugin;
  }

  interface ZoomSenseDataPlugins {
    "lightning-round": {
      [meetingId: string]: {
        [questionId: string]: {
          answers: SubmittedAnswer;
        };
      };
    };
    teamPlugin: {
      [meetingId: string]: Teams;
    };
  }
}

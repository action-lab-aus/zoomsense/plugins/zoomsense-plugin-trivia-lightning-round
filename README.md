# Spam Round plugin

This plugin allows teams to submit answers and zoomsense will automatically allocate points for correct and interact with the teams.
Each question will be its separate plugin.

** This plugin requires the use of the team plugin, the teams must have been allocated and set before using this plugin. **

## 2 Versions

There are 3 versions of this plugin:

### 1) Single Answer

In this version, only 1 correct answer can be submitted.
All the members in all teams can be submitting answers, if the answers are wrong, nothing happens.
This version is a race, the first team to submit the correct answer wins, once a correct answer has been submitted, no other answers will be accepted.
If someone answers one of the allowable answers, the zoombot will:

- let that team know they got the correct answer
- Increment the team's score
- message all other teams to tell them what the correct answer was and that they will now move on to the next round.
- automatically move to the next round

### 2) Trivia round

In this version, each team can submit correct answer(s) simultaneously.
This version is not a race against other teams, each team can get points for submitting the correct answer, and the round does not automatically move on once a correct answer has been submitted.
If someone answers a correct answer, the zoombot will:

- let that team know they got the correct answer
- Increment the team's score

If someone answers a correct answer that the team already submitted before the zoombot will:

- let that team know they have already submitted that answer

### 3) Race Round

In this version, it is the same as a single answer round, but we will not move on automatically to the next round, and we will not tell the other teams what the correct answer was.

If someone answers one of the allowable answers, the zoombot will:

- let that team know they got the correct answer
- Increment the team's score
- Decrement the weight of the question

## Config

use the `- plugin: lightning-round` option to enable this plugin.
The config setting field contains the following fields:

- roundName: the name of this question
- roundType: either "single_answer" or "multiple_answer"
- solutions: a dictionary containing the correct answer as a key, and an array of strings which represents the allowable answers for this question (because you may want to have multiple allowable answers for the same question such as abbreviations etc)
- questionWeight: the amount to increment the team's score by if they answer an answer correctly

The `demo_config.txt` in the root of this repo contains an example file for how to configure this plugin
